<?php

return [
    'dependencies' => [
        'invokables' => [
            Zend\Expressive\Router\RouterInterface::class => Zend\Expressive\Router\FastRouteRouter::class,
            App\Action\PingAction::class => App\Action\PingAction::class,
        ],
        'factories' => [
            App\Action\HomePageAction::class => App\Action\Factory\HomePageFactory::class,
            App\Action\LoginAction::class => App\Action\Factory\LoginActionFactory::class,
            App\Action\LogoutAction::class => App\Action\Factory\LogoutActionFactory::class,
        ],
    ],

    'routes' => [
        [
            'name' => 'home',
            'path' => '/',
            'middleware' => App\Action\HomePageAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'api.ping',
            'path' => '/api/ping',
            'middleware' => App\Action\PingAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'api.login',
            'path' => '/login',
            'middleware' => App\Action\LoginAction::class,
            'allowed_methods' => ['GET','POST'],
        ],
        [
            'name' => 'logout',
            'path' => '/logout',
            'middleware' => App\Action\LogoutAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'api.teste',
            'path' => '/teste/{session}',
            'middleware' => App\Action\LoginAction::class,
            'allowed_methods' => ['GET','POST'],
        ],
    ],
];
