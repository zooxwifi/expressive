<?php
namespace App\Action;

use Aura\Session\Session;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Helper\UrlHelper;
use Zend\Stratigility\MiddlewareInterface;

class LogoutAction implements MiddlewareInterface
{
    private $urlHelper;

    public function __construct(UrlHelper $urlHelper)
    {
        $this->urlHelper = $urlHelper;
    }

    /**
     * {@inheritDoc}
     * @see \Zend\Stratigility\MiddlewareInterface::__invoke()
     */
    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $session = $request->getAttribute(Session::class);
        $session->getSegment('auth')->clear();
        return new RedirectResponse($this->urlHelper->generate('home'));
    }
}