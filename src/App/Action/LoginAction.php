<?php
namespace App\Action;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Stratigility\MiddlewareInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use App\Form\Login;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Helper\UrlHelper;
use Aura\Session\Session;

class LoginAction implements MiddlewareInterface
{
    private $template;
    private $urlHelper;

    public function __construct(TemplateRendererInterface $template, UrlHelper $urlHelper)
    {
        $this->template = $template;
        $this->urlHelper = $urlHelper;
    }

    /**
     * {@inheritDoc}
     * @see \Zend\Stratigility\MiddlewareInterface::__invoke()
     */
    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $form = new Login();

        $session = $request->getAttribute(Session::class);

        $email = $session->getSegment('auth')->get('email', false);
        if ($email !== false) {
            return new RedirectResponse($this->urlHelper->generate('home'));
        }

        $num = $session->getSegment('auth')->get('num', 0);
        $session->getSegment('auth')->set('num', ++$num);

        if ($request->getMethod() == 'POST') {

            $post = $request->getParsedBody();
            $form->setData($post);

            if ($form->isValid()) {

                $data = $form->getData();
                $session->getSegment('auth')->set('email', $data['email']);

                return new RedirectResponse($this->urlHelper->generate('home'));

            }
        }
        return new HtmlResponse($this->template->render('app::login/index', [
            'id' => 2,
            'form' => $form,
            'layout' => 'layout/login',
            'num' => $num,
        ]));
    }
}