<?php
namespace App\Middleware\Factory;

use App\Middleware\SessionMiddleware;
use Aura\Session\Session;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class SessionMiddlewareFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     * @see \Zend\ServiceManager\Factory\FactoryInterface::__invoke()
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new SessionMiddleware($container->get(Session::class));
    }
}
