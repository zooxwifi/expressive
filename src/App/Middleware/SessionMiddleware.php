<?php
namespace App\Middleware;

use Aura\Session\Session;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

class SessionMiddleware implements MiddlewareInterface
{
    private $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * {@inheritDoc}
     * @see \Zend\Stratigility\MiddlewareInterface::__invoke()
     */
    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $request = $request->withAttribute(Session::class, $this->session);
        return $out($request, $response);
    }
}
