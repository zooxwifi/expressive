<?php
namespace App\View\Helper\Factory;

use Interop\Container\ContainerInterface;
use Zend\Form\ConfigProvider as FormConfigProvider;
use Zend\I18n\ConfigProvider as I18nConfigProvider;
use Zend\ServiceManager\Config;
use Zend\View\HelperPluginManager;

class HelperPluginManagerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->has('config') ? $container->get('config') : [];
        $config = isset($config['view_helpers']) ? $config['view_helpers'] : [];
        $manager = new HelperPluginManager($container);

        $manager->configure((new FormConfigProvider())->getViewHelperConfig());
        $manager->configure((new I18nConfigProvider())->getViewHelperConfig());
        $manager->configure($config);

        return $manager;
    }
}
