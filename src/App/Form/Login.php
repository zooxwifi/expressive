<?php
namespace App\Form;

use Zend\Filter;
use Zend\Form\Element\Button;
use Zend\Form\Element\Email;
use Zend\Form\Element\Password;
use Zend\Form\Element\Submit;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator;

class Login extends Form implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('loginForm');

        $this->add([
            'required' => true,
            'name' => 'email',
            'type' => Email::class,
            'options' => [
                'label' => 'Email',
            ],
            'attributes' => [
                'id' => 'email',
                'required' => true,
            ],
        ]);
        $this->add([
            'required' => true,
            'name' => 'password',
            'type' => Password::class,
            'options' => [
                'label' => 'Password',
            ],
            'attributes' => [
                'id' => 'password',
                'required' => true,
            ],
        ]);
        $this->add([
            'name' => 'submit-button',
            'type' => Submit::class,
            'options' => [
                'label' => 'Log In',
            ],
            'attributes' => [
                'id'    => 'submit-button',
            ],
        ]);
    }
    /**
     * {@inheritDoc}
     * @see \Zend\InputFilter\InputFilterProviderInterface::getInputFilterSpecification()
     */
    public function getInputFilterSpecification()
    {
        return [
            [
                'name' => 'email',
                'required' => true,
                'validators' => [
                    [
                        'name' => Validator\EmailAddress::class,
                    ],
                ],
                'filters' => [
                    ['name' => Filter\StringTrim::class],
                    [
                        'name' => Filter\StringToLower::class,
                        'options' => [
                            'encoding' => 'UTF-8',
                        ],
                    ],
                ],
            ],
            [
                'name' => 'password',
                'required' => true,
                'validators' => [
                    [
                        'name' => Validator\StringLength::class,
                        'options' => [
                            'min' => 6,
                        ]
                    ],
                ],
                'filters' => [
                    ['name' => Filter\StringTrim::class],
                ],
            ],
        ];
    }



}
